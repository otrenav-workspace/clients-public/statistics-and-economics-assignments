##
## Assignment 3
##
## Omar Trejo,
## February 2017
##

setwd(".")

require(ggplot2)

##
## Question 1
##

## b) Plot and solve

prices <- seq(0, 20)
revenue <- 30000 * prices - 1500 * prices^2
data <- data.frame(prices = prices, revenue = revenue)

## Plot revenue vs prices
ggplot(data, aes(x = prices, y = revenue)) +
    geom_line() +
    labs(title = "Q1-b) Revenue function")

## Maximum revenue:
prices[which.max(revenue)]

##
## Question 2
##

factor <- 247 / 30
prices <- seq(1000, 200, -50)
purchases <- c(1, 2, 1, 5, 6, 6, 13, 11, 12, 19, 17, 20, 22, 25, 29, 28, 30) * factor
revenue <- prices * purchases
data <- data.frame(prices = prices, purchases = purchases, revenue = revenue)

##
## a)
##
ggplot(data, aes(x = purchases, y = prices)) +
    geom_point() +
    labs(title = "Q2-a) Empirical demand")

##
## b)
##
ggplot(data, aes(x = purchases, y = revenue)) +
    geom_point() +
    labs(title = "Q2-b) Empirical revenue")

## Optimal purchases seem to be around 155. It's hard to tell
## the exact number by looking at the graph, but it's close
## to 155. Which would put optimal price around 450 (by
## looking at the graph from a) ). Optimal revenues seem to
## be around 85,000.

##
## c)
##
ggplot(data, aes(x = purchases, y = prices)) +
    geom_point() +
    geom_smooth(method = lm, se = FALSE) +
    labs(title = "Q2-c) Empirical and linear demand")

lm(purchases ~ prices, data)  ## => D(p) = 314.3196 - 0.3245 * p
summary(lm(purchases ~ prices, data))

## The quality of the regression is very high. We have a very
## high statistical significance level and it closely follows
## the data since there's a clear relation.

##
## d)
##

optimal_price <- 314.3196 / (2 * 0.3245)  ## From FOC  ## 484.3137
optimal_quantity <- 314.3196 - 0.3245 * optimal_price  ## 157.1598
optimal_revenues <- optimal_price * optimal_quantity   ## 76,114.65

## Getting this quantities from a graph, using the empirical demand
## and revenue functions, allows for better decisions when we are
## faced with non-linear data or when we have some intuition as to
## why the demand function behaved differently for a certain price
## rage. On the other hand, using the linear/fitted functions allows
## for more control on precision and can be automated. However we
## need to consider the drawbacks of linear models in this case.

##
## Question 3
##

data <- read.csv("./premodule_data.csv")

##
## a)
##
## NOTE: this is qualitative question that is open for interpretation
##
## Here's my take: we should treat items 1 and 7 differently since they
## show clearly different scenarios. In one of them the number of produced
## items were sold almost completely while the other sold barely over half.
## This means that they had different contexts or circumstances that affect
## demand behavior in each case. We need to take that into account when
## interpreting or using these data. We can say for example: for estimating
## the demand for this new item only use data for those items that were
## sold more than 80% (but this requires specific industry knowledge).
##

##
## b)
##
ggplot(data, aes(x = sold)) +
    geom_histogram(aes(y = ..density..)) +
    geom_density(color = "red") +
    labs(title = "Q3-b) Histogram and density")

## It does seem to follow a normal distribution

##
## c)
##
mean <- mean(data$sold)  ## 911.12
std  <- sd(data$sold)    ## 199.0874

##
## d)
##
## NOTE: I don't have access to OptQuest so I'll just do the
## confidence interval part of this question.
##

confidence_interval <- c(
    mean - 1.96 * std / sqrt(nrow(data)),
    mean + 1.96 * std / sqrt(nrow(data))
)

## 95% confidence interval: [ 855.9358, 966.3042 ]

##
## e)
##
excess_production <- mean(data$produced - data$sold)

## It seems that in average the company has produced 361.54
## more items than those which were sold, meaning that
## demand is constantly being overestimated. Which points towards
## producing a lower amount with respect to that which is
## estimated. However, when looking at the other information
## we have at hand:
##
## - Production and distribution costs: $80
## - Wholesale price: $120
## - Salvage price: $55
## - Goodwill loss for unmet demand: $45
##
## We can see that if we don't meet demand our loss will be of
## 80 - 55 + 45 = 70, which is larger than the loss of unsold
## items which is 120 - 55 = 65. This shows that the damage of
## not meeting demand is larger than the cost of having unsold
## items, which is probably why there's overproduction normally,
## and is correct to overproduce to mitigate risk.
##

##
## Question 4
##

## a) Can't answer this as I these are in-class related
## b) Can't answer this as I these are in-class related
