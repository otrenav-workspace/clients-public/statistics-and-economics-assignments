##
## Functions for analysis
##
## Omar Trejo
## March, 2017
##

require(lubridate)

MONTHS_SHORT <- list(
    "Jan" = "01",
    "Feb" = "02",
    "Mar" = "03",
    "Apr" = "04",
    "May" = "05",
    "Jun" = "06",
    "Jul" = "07",
    "Aug" = "08",
    "Sep" = "09",
    "Oct" = "10",
    "Nov" = "11",
    "Dec" = "12"
)

MONTHS_LONG <- list(
    "January"   = "01",
    "February"  = "02",
    "Marh"      = "03",
    "April"     = "04",
    "May"       = "05",
    "June"      = "06",
    "July"      = "07",
    "August"    = "08",
    "September" = "09",
    "October"   = "10",
    "November"  = "11",
    "December"  = "12"
)

clean <- function(data) {
    ##
    ## Clean data
    ##
    ## For now this function is just standarizing dates
    ##

    ## data$sale <- as.character(data$sale)
    ## data$artist <- as.character(data$artist)
    ## data$title <- as.character(data$title)
    data$saleDateClean <- standarize_dates(
        data$saleDate
    )
    data$auctionMonthDateClean <- standarize_auction_months(
        data$auctionMonthDate
    )
    variables_to_keep <- c(
        ## "sale",
        ## "saleTotal",
        ## "regionMapping",
        ## "currency",
        ## "estLowUSD",
        ## "estHighUSD",
        ## "saleYear",
        ## "saleMonth",
        "priceUSD",
        "saleDateClean",
        "auctionMonthDateClean",
        "artist",
        "title",
        "location",
        "category"
    )
    return(data[, variables_to_keep])
}

compute_title_statistics <- function(data) {
    title_statistics <- data.frame(
        "title" = NA,
        "n_observations" = NA,
        "min_date" = NA,
        "max_date" = NA,
        "min_price" = NA,
        "max_price" = NA
    )
    for (title in unique(data$title)) {
        data_for_title <- data[data$title == title, ]
        new_row <- data.frame(
            "title" = title,
            "n_observations" = nrow(data_for_title),
            "min_date" = min(data_for_title$saleDateClean),
            "max_date" = max(data_for_title$saleDateClean),
            "min_price" = min(data_for_title$priceUSD),
            "max_price" = max(data_for_title$priceUSD)
        )
        title_statistics <- rbind(title_statistics, new_row)
    }
    return(title_statistics[2:nrow(title_statistics), ])
}

standarize_dates <- function(dates) {
    ##
    ## We need dates in yyyy-mm-dd format to be useful for analysis
    ##
    standard_dates <- NA
    day_position <- 1
    year_position <- 3
    month_position <- 2
    for (date in dates) {
        elements <- strsplit(date, "-")
        if (length(elements[[1]]) == 3) {
            ##
            ## One type of dates in the data: 3-Nov-15
            ## - No initial zeros
            ## - No numeric months
            ## - No full years
            ##
            year <- fill_year(elements[[1]][year_position])
            month <- MONTHS_SHORT[[elements[[1]][month_position]]]
            day <- insert_initial_zero(elements[[1]][day_position])
        } else {
            ##
            ## The other type of dates: 3 November 2015 - 4 November 2015
            ## - Range of dates, not a single date
            ## - Non initial zeros
            ##
            first_date_elements <- strsplit(elements[[1]][1], " ")
            year <- first_date_elements[[1]][year_position]
            month <- MONTHS_LONG[[first_date_elements[[1]][month_position]]]
            day <- insert_initial_zero(first_date_elements[[1]][[day_position]])
        }
        date <- paste(year, "-", month, "-", day, sep = "")
        standard_dates <- c(standard_dates, date)
    }
    return(standard_dates[2:length(standard_dates)])
}

standarize_auction_months <- function(dates) {
    ##
    ## Instead of 1-Jan-16 for the auction month, we need a number
    ##
    standard_months <- NA
    month_position <- 2
    for (date in dates) {
        elements <- strsplit(date, "-")
        month <- as.integer(MONTHS_SHORT[[elements[[1]][month_position]]])
        standard_months <- c(standard_months, month)
    }
    return(standard_months[2:length(standard_months)])
}

insert_initial_zero <- function(value) {
    ##
    ## When we have 1 for january, we need 01 to have yyyy-mm-dd dates
    ##
    integer <- as.integer(value)
    if (integer < 10) {
        return(paste("0", integer, sep = ""))
    } else {
        return(as.character(integer))
    }
}

fill_year <- function(value) {
    ##
    ## It seems that all years composed of only two digits
    ## belong to the 2000's which is why I'm just prepending '20',
    ## but it may need an extension later on
    ##
    return(paste("20", value, sep = ""))
}

prepare_data_1 <- function(data) {
    ##
    ## Prepare data by translating dates into numerics
    ##
    data$saleDateNumeric <- assign_numerics_to_date(data$saleDateClean)
    return(data)
}

prepare_data_2 <- function(data) {
    ##
    ## Prepare data by translating dates into numerics and
    ## computing differences among min/max dates and prices
    ##
    data$max_date_numeric <- assign_numerics_to_date(data$max_date)
    data$min_date_numeric <- assign_numerics_to_date(data$min_date)
    data$date_difference <- data$max_date_numeric - data$min_date_numeric
    data$price_difference <- data$max_price - data$min_price
    return(data)
}

assign_numerics_to_date <- function(dates) {
    return(as.numeric(ymd(dates)))
}

generate_new_dataset <- function(data) {
    ##
    ## Create new data set
    ##
    ## - I'm assuming that the artist and cateogry don't change
    ## - Min/max location corresponds to the min/max auction date
    ##
    data <- data[complete.cases(data), ]
    new_data <- data.frame(
        "title" = NA,
        "artist" = NA,
        "category" = NA,
        "n_observations" = NA,
        "n_distinct_locations" = NA,
        "min_location" = NA,
        "max_location" = NA,
        "min_date" = NA,
        "max_date" = NA,
        "min_price" = NA,
        "max_price" = NA
    )
    for (title in unique(data$title)) {
        data_for_title <- data[data$title == title, ]
        min_date_index <- which.min(as.numeric(ymd(data_for_title$saleDateClean)))
        max_date_index <- which.max(as.numeric(ymd(data_for_title$saleDateClean)))
        new_row <- data.frame(
            "title" = title,
            "artist" = data_for_title[1, "artist"],
            "category" = data_for_title[1, "category"],
            "n_observations" = nrow(data_for_title),
            "n_distinct_locations" = length(unique(data_for_title$location)),
            "min_location" = data_for_title[min_date_index, "location"],
            "max_location" = data_for_title[max_date_index, "location"],
            "min_date" = data_for_title[min_date_index, "saleDateClean"],
            "max_date" = data_for_title[max_date_index, "saleDateClean"],
            "min_price" = min(data_for_title$priceUSD),
            "max_price" = max(data_for_title$priceUSD)
        )
        new_data <- rbind(new_data, new_row)
    }
    new_data <- new_data[2:nrow(new_data), ]
    return(prepare_data_2(new_data[new_data$n_observations > 1, ]))
}
