
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Statistics and Economics Assignments

- Omar Trejo
- February, 2017

The client was assigned three assignments that had to do with economis and
statistics. She was asked to use provide code and mathematical explanations
where necessary. This is my attempt to solve the assignments to make sure the
client understand the concepts and techniques necessary to fulfill the
assignments successfully.

---

> "The best ideas are common property."
>
> —Seneca
